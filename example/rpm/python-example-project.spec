%define srcname example-project
%define distname %{lua:name = string.gsub(rpm.expand("%{srcname}"), "-", "_"); print(name)}
%define version 0.0.0
%define release 1

Name:     python-%{srcname}
Version:  %{version}
Release:  %{release}%{?dist}
Summary:  Example project for Red Hat CI/CD component

License:  MIT
Url:      https://git.ligo.org/computing/gitlab/components/redhat
Source0:  %pypi_source %distname

Packager: Duncan Macleod <duncan.macleod@ligo.org>
Vendor:   Duncan Macleod <duncan.macleod@ligo.org>

BuildArch: noarch

%if 0%{?rhel} == 0 || 0%{?rhel} >= 9
BuildRequires: pyproject-rpm-macros
%endif
BuildRequires: python3-devel >= 3.6
BuildRequires: python3dist(pip)
BuildRequires: python3dist(setuptools)
BuildRequires: python3dist(setuptools-scm)
BuildRequires: python3dist(wheel)
# for tests
BuildRequires: python3dist(pytest)
BuildRequires: python3dist(requests-mock)

%description
This package is an example project binary package that should not be
distributed or installed on any systems save for the purpose of testing
and validation of the parent GitLab CI/CD component project.

%package -n python3-%{srcname}
Summary: Example project for Red Hat CI/CD component - Python %{python3_version} library
%description -n python3-%{srcname}
Python %{python3_version} library for example project.

%prep
%autosetup -n %{distname}-%{version}
# for RHEL < 10 hack together setup.{cfg,py} for old setuptools
%if 0%{?rhel} && 0%{?rhel} < 10
cat > setup.cfg << SETUP_CFG
[metadata]
name = %{srcname}
version = %{version}
author-email = %{packager}
description = %{summary}
license = %{license}
license_files = LICENSE
url = %{url}
[options]
packages = find:
python_requires = >=3.5
install_requires =
	requests
SETUP_CFG
%endif

%if %{undefined pyproject_wheel}
cat > setup.py << SETUP_PY
from setuptools import setup
setup()
SETUP_PY
%endif

%build
%if %{defined pyproject_wheel}
%pyproject_wheel
%else
%py3_build_wheel
%endif

%install
%if %{defined pyproject_install}
%pyproject_install
%else
%py3_install_wheel %{distname}-%{version}-*.whl
%endif

%check
export PYTHONPATH="%{buildroot}%{python3_sitelib}"
%{__python3} -m pip show %{srcname} -f
%{__python3} -m pytest --verbose -ra --pyargs example_project

%files -n python3-%{srcname}
%license LICENSE
%doc README.md
%{python3_sitelib}/*

%changelog
* Fri Apr 26 2024 Duncan Macleod <duncan.macleod@ligo.org> - 0.0.0-1
- initial build of example project
