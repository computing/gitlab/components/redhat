# CI/CD components for Red Hat

This project provides [CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
for a software project that includes Red Hat build, test, and quality checking.

## Usage

See the online documentation at

<https://computing.docs.ligo.org/guide/gitlab/components/redhat/>

## Contributing

Please read about CI/CD components and best practices at: <https://git.ligo.org/help/ci/components/index.html>.

All interactions related to this project should follow the
[LIGO-Virgo-KAGRA Code of Conduct](https://dcc.ligo.org/LIGO-M1900037).

For more details on contributing to this project, see `CONTRIBUTING.md`.
