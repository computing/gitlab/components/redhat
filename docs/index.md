# RedHat CI/CD components

This `computing/gitlab/components/redhat` project provides
[CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
for a software project that includes RPM build, test, and quality checking.

## Latest release

[![latest badge](https://git.ligo.org/computing/gitlab/components/redhat/-/badges/release.svg?text=test)](https://git.ligo.org/explore/catalog/computing/gitlab/components/redhat/ "See latest release in the CI/CD catalog")

## Components

The following components are available:

- [`redhat/source`](./source.md "`redhat/source` component documentation")
- [`redhat/build`](./build.md "`redhat/build` component documentation")
- [`redhat/test`](./test.md "`redhat/test` component documentation")

In addition the following meta-components (combinations) of the above components
are available:

- [`redhat/all`](./all.md "`redhat/all` component documentation)
