# `redhat/test`

Configure jobs to test newly built RPMs.

## Description

This component creates multiple jobs, each with a common prefix, that
take in newly built binary RPMs (downloaded as artifacts of other jobs
in the pipeline), bundle them into a local Yum repository, and install
them before executing the user-specific test commands.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/redhat/test@<VERSION>
    inputs:
      test_script:
        # sanity check executable installed from RPM
        - /usr/bin/my-script --help
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `build_prefix` | `redhat_build` {: .nowrap } | Job name prefix used for build jobs on which to depend. |
| `cache_dir` | `".cache/rpm"` {: .nowrap } | The path to cache downloaded RPMs to (relative to `$CI_PROJECT_DIR`) |
| `disable_repos` {: .nowrap } | `""` | Space-separate list of RPM repo names to disable |
| `dnf_update` | `true` | Update all installed packages with `dnf update` before proceeeding |
| `enable_repos` | `""` | Space-separate list of RPM repo names to enable |
| `epel` | `false` | Pre-configure the EPEL (Extra Packages for Enterprise Linux) repository |
| `git_strategy` | `fetch` | Value for [`GIT_STRATEGY`](https://git.ligo.org/help/ci/runners/configure_runners.html#git-strategy). |
| `job_prefix` | `redhat_test` | Prefix to use for job name. |
| `redhat_versions` {: .nowrap } | `[8]` | RedHat versions to test on. |
| `stage` | `test` | The stage to add jobs to |
| `test_install` | `""` | Packages to install to support the `test_script`. |
| `test_script` | | Script commands to run as part of the test jobs. |

## Notes

### Test jobs require matching jobs from `redhat/build` { #build-and-test }

`redhat/test` jobs are automatically configured with `needs` referencing
the job from [`redhat/build`](./build.md) with the same `redhat_version` value.
It is required that when configuring the `redhat/test` you also configure the
`redhat/build` component.

See [_Examples_](#examples) for examples.

### Test reports { #reports }

The test jobs configured by `redhat/test` include
[`artifacts:report`](https://git.ligo.org/help/ci/yaml/artifacts_reports.html)
as follows:

```yaml
  artifacts:
    reports:
      # coverage report
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
      # test report
      junit: "**/*junit*.xml"
    paths:
      - "**/.coverage*"
```

This is done to help projects automatically receive test reports and coverage
information on merge request pages.

For projects that do not generate these reports, this will result in the
following output at the end of the build log:

``` { .text .no-copy }
Uploading artifacts...
WARNING: coverage.xml: no matching files. Ensure that the artifact path is relative to the working directory (/builds/duncanmmacleod/component-redhat).
ERROR: No files to upload
```

with the `ERROR` line highlighted in red.
These 'errors' are not fatal and can be safely ignored.

To remove the custom artifacts, override the artifacts setting for the
`redhat_test` job configuration

```yaml
redhat_test: # (1)!
  artifacts: null
```

1.  The `redhat_test` job name should modified to match whatever you set as
    the `job_prefix` input when including the `redhat/test` component.

### Test jobs do not automatically install built packages { #no-local-install }

The `redhat/test` jobs find all RPMs brought in as artifacts and bundle them
into a local Yum repository.
However, starting from __version `2.0.0`__ of this component, the jobs
configured by the `redhat/test` component do ___not___ automatically install
_any_ RPMs; this includes any RPMs included in the local Yum repository.

To ensure that built packages are installed for testing, include the
appropriate package names in the `test_install` input.

## Customisation

All customisation should be achieved through the [`inputs`](#inputs),
or by overriding the test job template.

See [_Customimse the test job template_](#test) under [_Examples_](#examples)
below for more details.

## Examples

### Build and test a Python project {: #python }

!!! example "Build and test a Python application on RedHat"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/sdist@0.1
        inputs:
          stage: source
      - component: git.ligo.org/computing/gitlab/components/redhat/build@<VERSION>
        inputs:
          needs: [sdist]
      - component: git.ligo.org/computing/gitlab/components/redhat/test@<VERSION>
        inputs:
          test_install: python3-pytest
          test_script:
            - python3 -m pytest --pyargs my_library.tests
    ```

### Customise the test job template {: #test }

!!! example "Customise the test job template"

    This example demonstrates how to heavily customise the `redhat_test` job
    template used for testing of all `redhat_versions`, including adding
    custom artifacts for test and coverage reporting for a Python project.

    ```
    include:
      - component: git.ligo.org/computing/gitlab/components/python/sdist@0.1
        inputs:
          stage: source
      - component: git.ligo.org/computing/gitlab/components/redhat/build@<VERSION>
        inputs:
          needs: [sdist]
      - component: git.ligo.org/computing/gitlab/components/redhat/test@<VERSION>
        inputs:
          test_install: >-
            python3
            python3-mylibrary
            python3-pytest
            python3-pytest-cov
          test_script:
            - python3 -m pytest --junit-xml=junit.xml --pyargs my_library.tests --cov my_library
            - python3 -m coverage xml -o coverage.xml

    # customise the template used for the version-specific test jobs
    redhat_test:
      artifacts:
        reports:
          coverage_report:
            coverage_format: cobertura
            path: coverage.yml
          junit: junit.xml
    ```

    The customisations are
    [merged](https://git.ligo.org/help/ci/yaml/yaml_optimization.html#merge-details)
    with the template configured by the `computing/gitlab/components/redhat/test`
    component, so anything that isn't specified manually is preserved from the
    template.
