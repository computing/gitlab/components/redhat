# `redhat/all`

Configure an end-to-end RPM build-and-test workflow for this project.

## Description

This component automatically configures a workflow to

- build one-or-more [binary RPMs](./build.md)
- install and test the new binary packages

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/redhat/all@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `cache_dir` | `".cache/rpm"` {: .nowrap } | The path to cache downloaded RPMs to (relative to `$CI_PROJECT_DIR`) |
| `disable_repos` {: .nowrap } | `""` | Space-separate list of RPM repo names to disable |
| `dnf_update` | `true` | Update all installed packages with `dnf update` before proceeeding |
| `enable_repos` | `""` | Space-separate list of RPM repo names to enable |
| `epel` | `false` | Pre-configure the EPEL (Extra Packages for Enterprise Linux) repository |
| `job_prefix` | `"redhat"` | Prefix to apply to all job names |
| `needs` | | List of jobs whose artifacts are required to create the source RPM (e.g. the job that created the project source distribution)"
| `redhat_versions` | `[8]` | Set of RedHat versions to build/test |
| `rpmbuild_options` | `""` | Options to pass to `rpmbuild` |
| `rpmlint_options` | `""` | Options to pass to `rpmlint` |
| `source_distribution_name` | `"*.tar.*"` | Name of the source distribution file |
| `test_install` | `""` | Extra packages to install to support the `test_script` |
| `test_script` | | Script commands to run as part of the redhat test jobs |

## Notes

### Requires an upstream source distribution

Creating RPM packages is only supported when starting from an
upstream tarball that contains a `.spec` file.

This must be configured independently of this component and the tarball
provided to the `redhat/all` component via the `needs` input.
See [_Examples_](#examples) for an example.

## Examples

### Build and test a Python project {: #python }

!!! example "Build and test a Python application on RedHat"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/sdist@0.1
        inputs:
          stage: source
      - component: git.ligo.org/computing/gitlab/components/redhat/all@<VERSION>
        inputs:
          needs: [sdist]
          test_install: python3-pytest
          test_script:
            - python3 -m pytest --pyargs my_library.tests
    ```
