# `redhat/build`

Configure jobs to build binary RPMs (`.rpm`) for this project
from a source distribution or [source RPM](./source.md).

## Description

This component creates multiple jobs -- one for each chosen RedHat version --
each with a common prefix, that take in a pre-existing upstream source
distribution (tarball) or source RPM, and use
[`rpmbuild`](https://www.redhat.com/sysadmin/create-rpm-package)
to create binary RPMs.

After each build `rpmlint` is used to scan the new RPMs to produce a GitLab
[Code Quality report](https://git.ligo.org/help/ci/testing/code_quality.html).

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/redhat/build@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `cache_dir` | `".cache/rpm"` {: .nowrap } | The path to cache downloaded RPMs to (relative to `$CI_PROJECT_DIR`) |
| `disable_repos` {: .nowrap } | `""` | Space-separate list of RPM repo names to disable |
| `dnf_update` | `true` | Update all installed packages with `dnf update` before proceeeding |
| `enable_repos` | `""` | Space-separate list of RPM repo names to enable |
| `epel` | `false` | Pre-configure the EPEL (Extra Packages for Enterprise Linux) repository |
| `job_prefix` | `redhat_build` {: .nowrap } | Prefix to use for build job name |
| `needs` | `[]` | List of jobs whose artifacts are needed for the RPM build |
| `redhat_versions` | `[8]` | Versions of Red Hat to build for |
| `dynamic_version` | `true` | Automatically detect package version from Python `PKG-INFO` metadata file and update RPM `.spec` file |
| `rpmbuild_options` | `""` | Options to pass to rpmbuild |
| `rpmlint_options` | `""` | Options to pass to `rpmlint` |
| `source_distribution_name` {: .nowrap } | `"*.tar.*"` | Name of the source distribution file (if building directly from a tarball) |
| `srpm_name` | `"*.src.rpm"` | Name of the source RPM file |
| `stage` | `build` | The stage to add jobs to |

## Notes

### Requires an upstream source distribution or source RPM

Creating binary RPMs is only supported when starting from an upstream tarball
that contains a `.spec` file, **OR** a source RPM (`.src.rpm`).

See [_Examples_](#examples) for examples.

## Customisation

### Rpmlint { #rpmlint data-search-exclude }

See [_Rpmlint_](./source.md#rpmlint) in `redhat/source` for details.

## Examples

### Build a binary RPM for a Python project {: #python }

!!! example "Create binary RPMs from a Python 'sdist'"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/sdist@0.1
        inputs:
          stage: source
      - component: git.ligo.org/computing/gitlab/components/redhat/build@<VERSION>
        inputs:
          needs: [sdist]
    ```
