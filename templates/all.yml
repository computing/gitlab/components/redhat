# -- Meta-component to run end-to-end pipeline for RedHat

spec:
  inputs:
    # -- common inputs

    cache_dir:
      default: ".cache/rpm"
      description: "The path to cache downloaded RPMs to (relative to CI_PROJECT_DIR)"
    disable_repos:
      default: ""
      description: "Space-separate list of RPM repo names to disable"
      type: string
    dnf_update:
      default: true
      description: "Update all packages with 'dnf update' before proceeding"
      type: boolean
    enable_repos:
      default: ""
      description: "Space-separate list of RPM repo names to enable"
      type: string
    epel:
      default: false
      description: "Pre-configure the EPEL (Extra Packages for Enterprise Linux) repository"
      type: boolean

    # -- all pipeline options

    job_prefix:
      default: "redhat"
      description: "Prefix to apply to all job names"
      type: string
    redhat_versions:
      default:
        - 8
      description: "Set of Red Hat versions to build/test"
      type: array
    needs:
      description: "List of jobs whose artifacts are required to create the source RPM (e.g. the job that created the project source distribution)"
      type: array
    source_distribution_name:
      default: "*.tar.*"
      description: "Name of the source distribution file"
      type: string
    spec_file:
      default: "*.spec*"
      description: "Path of/pattern for the RPM spec file"
      type: string
    dynamic_version:
      default: true
      description: "Automatically detect package version from Python PKG-INFO file and update spec file to match"
      type: boolean
    rpmbuild_options:
      default: ""
      description: "Options to pass to rpmbuild"
      type: string
    rpmlint_options:
      default: ""
      description: "Options to pass to rpmlint"
      type: string
    test_install:
      default: ""
      description: "Extra packages to install to support the test_script"
      type: string
    test_script:
      default:
        - |
          # no script command passed, error
          echo -e "\x1B[91mError: no inputs:test_script passed to include, or script commands provided as an override to the redhat/test job template\x1B[0m"
          exit 1
      description: "Script commands to run as part of the test jobs"
      type: array

---

include:
  - local: templates/build.yml
    inputs:
      cache_dir: $[[ inputs.cache_dir ]]
      disable_repos: $[[ inputs.disable_repos ]]
      enable_repos: $[[ inputs.enable_repos ]]
      epel: $[[ inputs.epel ]]
      stage: build
      redhat_versions: $[[ inputs.redhat_versions ]]
      job_prefix: &build_prefix "$[[ inputs.job_prefix ]]_build"
      needs: $[[ inputs.needs ]]
      source_distribution_name: "$[[ inputs.source_distribution_name ]]"
      spec_file: "$[[ inputs.spec_file ]]"
      dynamic_version: $[[ inputs.dynamic_version ]]
      rpmbuild_options: "$[[ inputs.rpmbuild_options ]]"
      rpmlint_options: "$[[ inputs.rpmlint_options ]]"
  - local: templates/test.yml
    inputs:
      cache_dir: $[[ inputs.cache_dir ]]
      disable_repos: $[[ inputs.disable_repos ]]
      enable_repos: $[[ inputs.enable_repos ]]
      epel: $[[ inputs.epel ]]
      stage: test
      redhat_versions: $[[ inputs.redhat_versions ]]
      build_prefix: *build_prefix
      job_prefix: "$[[ inputs.job_prefix ]]_test"
      test_install: $[[ inputs.test_install ]]
      test_script: $[[ inputs.test_script ]]
